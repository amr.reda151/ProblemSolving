/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package A;

import java.util.ArrayList;

/**
 * 289A
 *
 * @author amr
 */
public class PenguinAndSegments {

    public static void main(String[] args) {
        mySol();
    }

    private static void mySol() {
        ArrayList<Integer> list = new ArrayList<>();

        int[][] segments = {
            {1, 2},
            {3, 4},
            {5, 6}
        };
        int k = 6;

        for (int i = 0; i < segments.length; i++) {
            for (int j = 0; j < segments[i].length; j++) {
                if (!check(list, segments[i][j])) {
                    list.add(segments[i][j]);
                }
            }
        }
        if (k >= list.size()) {
            System.out.println(k - list.size());
        } else {
            System.out.println(Math.abs((list.size() % k) - k));
        }

    }

    private static boolean check(ArrayList<Integer> list, int num) {
        for (int i : list) {
            if (i == num) {
                return true;
            }
        }

        return false;
    }
}
