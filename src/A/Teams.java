/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package A;

import java.util.Scanner;
import jdk.nashorn.internal.parser.Lexer;

/**
 *231A
 * @author amr
 */
public class Teams {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = 0, answer = 0;
        n = in.nextInt();
        
        for(int i = 0; i < n; i++) {
            String line = in.next();
            
            if(getInt(line, 0) + getInt(line, 1) + getInt(line, 2) >= 2) {
                answer++;
            }
        }
        
        System.out.println("answer is " + answer);
    }
    
    private static int getInt(String line, int i) {
        return Integer.parseInt(String.valueOf(line.charAt(i)));
    }
}
