/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package A;

import java.util.Scanner;

/**
 *263A
 * @author amr
 * 00000
 * 00000
 * 00100
 * 00000
 * 00000
 */
public class BeutifullMatrix {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        int currentIndex = -1, midIndex = -1, rowSize = 5, colSize = 5;
        
        int[][] matrix = new int[rowSize][colSize];
        
        midIndex = Math.floorDiv((rowSize * colSize ), 2);
        
        outerLoop : for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[i].length; j++) {
                currentIndex++;
                System.out.print((currentIndex + 1) + ": ");
                matrix[i][j] = in.nextInt();
                
                if(matrix[i][j] == 1) {
                    break outerLoop;
                }
                
                System.out.println();
            }
        }
        
        System.out.println("answer is " + Math.abs(midIndex - currentIndex));
        
    }
}