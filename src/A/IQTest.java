/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package A;

import java.util.Scanner;

/**
 * 287A
 *
 * @author amr
 */
public class IQTest {

    private static char[][] p = new char[4][4];

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        anotherSol(in);
    }

    private static void mySol(Scanner in) {

        boolean change = true;
        int nHashs = 0, nPoints = 0, index = -1;

        for (int i = 0; i < p.length; i++) {
            for (int j = 0; j < p[i].length; j++) {
                index++;
                System.out.print(index + ": ");
                p[i][j] = in.next().charAt(0);
                if (p[i][j] == '#') {
                    nHashs++;
                } else {
                    nPoints++;
                }
                System.out.println();
            }

            if (nHashs - nPoints == 2 || nPoints - nHashs == 2) {
                if (change) {
                    change = false;
                } else {
//                    System.out.println("Something went wrong!");
                    break;
                }
            }

            nHashs = 0;
            nPoints = 0;
        }

        if (!change) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }

    }

    private static void anotherSol(Scanner in) {
        int index = -1;

        for (int i = 0; i < p.length; i++) {
            for (int j = 0; j < p[i].length; j++) {
                index++;
                System.out.print(index + ": ");
                p[i][j] = in.next().charAt(0);
            }
        }

        boolean done = test();

        for (int i = 0; i < p.length; i++) {
            for (int j = 0; j < p[i].length; j++) {
                char point = p[i][j];
                
                if (p[i][j] == '#') {
                    p[i][j] = '.';
                } else {
                    p[i][j] = '#';
                }
                
                if(test()) {
                    done = true;
                }
                
                p[i][j] = point;
            }
        }
        
        if(done) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }

    private static boolean test() {
        for (int i = 0; i < p.length; i++) {
            for (int j = 0; j < p[i].length; j++) {
                if (test(i, j)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean test(int i, int j) {
        if (i + 1 == 4 || j + 1 == 4) {
            return false;
        }

        return p[i][j] == p[i + 1][j] && p[i][j] == p[i][j + 1] && p[i][j] == p[i + 1][j + 1];
    }
}
