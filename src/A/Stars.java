/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package A;

/**
 *
 * @author amr
 */
public class Stars {
    public static void main(String[] args) {
        mStars();
    }

    private static void mStars() {
        int count = 3, stars = (count - 1) / 2;
        
        for (int tmp = 1; tmp <= count; tmp += 2) {
            for(int j = 1; j <= stars; j++) {
                System.out.print("*");
            }
            
            for (int i = 0; i < tmp; i++) {
                System.out.print("D");
            }
                        
            for(int j = 1; j <= stars; j++) {
                System.out.print("*");
            }
            
            System.out.println("");
            
            stars--;
        }
        
        stars = 0;
        
        for (int tmp = (count - 2); tmp >= 1; tmp -= 2) {
            stars++;
            
            for(int j = 1; j <= stars; j++) {
                System.out.print("*");
            }
            
            for (int i = 0; i < tmp; i++) {
                System.out.print("D");
            }
                        
            for(int j = 1; j <= stars; j++) {
                System.out.print("*");
            }
            
            System.out.println("");
            
        }
    }
}
