/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package A;

/**
 *
 * @author amr
 */
public class Series {
    public static void main(String[] args) {
        mSeries();
    }
    
    private static void mSeries() {
        int n = 3, num1 = 0, num2 = 1, sum = 0;
        if(n == 1) {
            System.out.println("0");
        } else {
            System.out.print("0 1 ");
            for (int i = 0; i < n - 2; i++) {
                sum = num1 + num2;
                
                System.out.print(sum + " ");
                
                num1 = num2;
                num2 = sum;
            }
        }
    }
}
